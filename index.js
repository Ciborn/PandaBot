const Discord = require('discord.js');
const bot = new Discord.Client();
const youtubeStream = require('youtube-audio-stream');
const ytdl = require('ytdl-core');
const multiMap = require('multimap');
var request = require('request');

(function(){
	var alphabet = { a:/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/ig,
		aa:/[\uA733]/ig,
		ae:/[\u00E6\u01FD\u01E3]/ig,
		ao:/[\uA735]/ig,
		au:/[\uA737]/ig,
		av:/[\uA739\uA73B]/ig,
		ay:/[\uA73D]/ig,
		b:/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/ig,
		c:/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/ig,
		d:/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/ig,
		dz:/[\u01F3\u01C6]/ig,
		e:/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/ig,
		f:/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/ig,
		g:/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/ig,
		h:/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/ig,
		hv:/[\u0195]/ig,
		i:/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/ig,
		j:/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/ig,
		k:/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/ig,
		l:/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/ig,
		lj:/[\u01C9]/ig,
		m:/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/ig,
		n:/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/ig,
		nj:/[\u01CC]/ig,
		o:/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/ig,
		oi:/[\u01A3]/ig,
		ou:/[\u0223]/ig,
		oo:/[\uA74F]/ig,
		oe:/[\u0153]/ig,
		p:/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/ig,
		q:/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/ig,
		r:/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/ig,
		s:/[\u0073\u24E2\uFF53\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/ig,
		ss:/[\u00DF\u1E9E]/ig,
		t:/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/ig,
		tz:/[\uA729]/ig,
		u:/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/ig,
		v:/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/ig,
		vy:/[\uA761]/ig,
		w:/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/ig,
		x:/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/ig,
		y:/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/ig,
		z:/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/ig,
		'':/[\u0300\u0301\u0302\u0303\u0308]/ig
	};
	replaceDiacritics = function(str) {
		for (var letter in alphabet) {
			str = str.replace(alphabet[letter], letter);
		}	
		return str;
	};
}());

var musicInitiated = new Array();
var musicsAwaiting = new Map();
var blindTestMode = new Map();

function entierAleatoire(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function ytPlay(ytURI, voiceChannel, message) {
	if (blindTestMode.get(voiceChannel.guild.id) != undefined) {
		console.log('4');

		voiceChannel.join().then(function(connection) {
			var stream = youtubeStream(ytURI);
			
			const embed = new Discord.RichEmbed()
			.setAuthor("Musique PandaBot (Blind Test)", bot.user.avatarURL)
			.setTitle(`YouTube`)
			.setDescription(`Les informations de cette vidéo seront affichés à sa fin.`)
			.setColor("#279AF1")
			.setFooter(message.author.username, message.author.avatarURL)
			blindTestMode.get(voiceChannel.guild.id).send({embed}).then(function(message) {
				connection.playStream(stream, ['0', '1', '1', 'auto']).on('end', function() {
					ytdl.getInfo(ytURI, [], function(err, info) {
						const embed = new Discord.RichEmbed()
						.setAuthor("Musique PandaBot (Blind Test)", bot.user.avatarURL)
						.setTitle(info.title)
						.setDescription(info.author.name)
						.setThumbnail(info.thumbnail_url)
						.setColor("#279AF1")
						.setFooter(message.author.username, message.author.avatarURL)
						message.edit({embed});
					})
	
					connection.disconnect();
				})
			})
		})
	} else {
		var voiceChannel = message.member.voiceChannel;
	
		voiceChannel.join().then(function(connection) {
			var stream = youtubeStream(ytURI);
			connection.playStream(stream, ['0', '1', '1', 'auto']).on('end', function() {
				if (musicsAwaiting.has(message.guild.id)) {
					ytPlay(musicsAwaiting.get(message.guild.id), voiceChannel, message);
					musicsAwaiting.delete(message.guild.id);
				} else {
					connection.disconnect();
					musicInitiated.splice(musicInitiated.indexOf(message.guild.id), 1);
					musicsAwaiting.delete(message.guild.id);
				}
			})

			ytdl.getInfo(ytURI, [], function(err, info) {
				const embed = new Discord.RichEmbed()
				.setAuthor("Musique PandaBot", bot.user.avatarURL)
				.setTitle(info.title)
				.setDescription(info.author.name)
				.setThumbnail(info.thumbnail_url)
				.setColor("#279AF1")
				.setFooter(message.author.username, message.author.avatarURL)
				message.channel.send({embed});
			})
		})
	}
}

/*
process.on('uncaughtException', console.error);
process.on('warning', console.error);*/

bot.on('ready', () => {
	bot.user.setStatus('online');
	bot.user.setGame('pb!play [lien] | Par Ciborn');
	//bot.guilds.find('name', 'Ciborn Bots Server').members.find('id', '320933389513523220').send(`**C'est bon PandaBot est connecté !**`);
});

bot.login('MzcyMDc0NTk0NDM1MDcyMDAy.DM-4_A.i7GO6dD_EgWJAq6CS4H1M0_y1WA');

bot.on('guildCreate', guild => {
    guild.channels.find('name', 'general').send(`Sympa comme endroit... C'est **${guild.owner.user.username}** qui gère les lieux ? J'espère qu'il a pas oublié mon bambou...`);
});

bot.on('message', message => {
	var messageReduced = replaceDiacritics(message.content);
	if (messageReduced.indexOf('pb!stop') == 0 && message.channel.type == 'text') {
		try {
			message.member.voiceChannel.leave();
			musicInitiated.splice(musicInitiated.indexOf(message.guild.id), 1);
			musicsAwaiting.delete(message.guild.id);
		} catch (err) {
			message.channel.send(`Ah, je n'ai pas pu stopper ta musique...`);
		}
	} else if (messageReduced.indexOf('pb!play') == 0 && message.channel.type == 'text') {
		var stringFind = message.content.indexOf('https://youtube.com/') + 29;
		var stringSub = message.content.substr(stringFind, 11);
		var stringFull = 'https://youtube.com/' + stringSub;
		var stringOnly = message.content.substr(8, 43);
		if (musicInitiated.indexOf(message.guild.id) == -1) {
			try {
				ytPlay(stringOnly, message.member.voiceChannel, message);
				message.delete();
			} catch (err) {
				message.channel.send(`Une erreur est survenue ${message.author.username}. J'ai déjà tout dit à Ciborn. Pour t'aider à résoudre le problème rapidement, assure-toi que tu étais dans un channel vocal et que ton lien ne pointait pas vers un live.`);
				
				const embed = new Discord.RichEmbed()
				.setAuthor("Erreur PandaBot", bot.user.avatarURL)
				.setTitle(`Une erreur est survenue. Détails ci-dessous.`)
				.setDescription(`\`\`${err}\`\``)
				.setColor("#C73E1D")
				.addField(`Serveur`, `${message.guild.name} : ${message.guild.id}`)
				.addField(`Message`, message.content)
				.setFooter(`${message.author.username} : ${message.author.id}`, message.author.avatarURL);
				//bot.guilds.find('name', 'Ciborn Bots Server').members.find('id', '320933389513523220').send({embed});
			}
		} else {
			if (musicsAwaiting.has(message.guild.id)) {
				const embed = new Discord.RichEmbed()
				.setAuthor("Erreur PandaBot", bot.user.avatarURL)
				.setTitle(`PandaBot ne peut pas gérer une file d'attente possédant plus d'un morceau.`)
				.setColor("#C73E1D")
				.setFooter(message.author.username, message.author.avatarURL)
				message.channel.send({embed});
				message.delete();
			} else {
				message.delete();
				
				const embed = new Discord.RichEmbed()
				.setAuthor("Musique PandaBot", bot.user.avatarURL)
				.setTitle(`Morceau mis en attente`)
				.setDescription(stringOnly)
				.setColor("#BC8034")
				.setFooter(message.author.username, message.author.avatarURL)
				message.channel.send({embed});
				musicsAwaiting.set(message.guild.id, stringOnly);
			}
		}
	} else if (messageReduced.indexOf('pb!search') == 0 && message.channel.type == 'text') {
		messageLengthReduced = message.content.length - 10;
		var ytSearch = messageReduced.substr(10, messageLengthReduced);

		var videoInfo = function(ytSearch, callback) {
			var options = {
				url: 'https://www.googleapis.com/youtube/v3/search?part=snippet&key=AIzaSyAGzso9kLcBTw8Ycovpe01N7nIhtrbTuL4&q=' + ytSearch,
			};
			request(options, function(err, response, body) {
				try {
					var result = JSON.parse(body);
					var videoID = result.items[0].id.videoId;
					callback(null, videoID);
				} catch(e) {
					callback(e); 
				}
			});
		}

		videoInfo(ytSearch, function(err, videoID) {
			if (musicInitiated.indexOf(message.guild.id) == -1) {
				try {
					ytPlay(videoID, message.member.voiceChannel, message);
				} catch (err) {
					message.channel.send(`Une erreur est survenue ${message.author.username}. J'ai déjà tout dit à Ciborn. Pour t'aider à résoudre le problème rapidement, assure-toi que tu étais dans un channel vocal et que ton lien ne pointait pas vers un live.`);
					
					const embed = new Discord.RichEmbed()
					.setAuthor("Erreur PandaBot", bot.user.avatarURL)
					.setTitle(`Une erreur est survenue. Détails ci-dessous.`)
					.setDescription(`\`\`${err}\`\``)
					.setColor("#C73E1D")
					.addField(`Serveur`, `${message.guild.name} : ${message.guild.id}`)
					.addField(`Message`, message.content)
					.setFooter(`${message.author.username} : ${message.author.id}`, message.author.avatarURL)
					bot.guilds.find('name', 'Ciborn Bots Server').members.find('id', '320933389513523220').send({embed});
				}
			} else {
				if (musicsAwaiting.has(message.guild.id)) {
					const embed = new Discord.RichEmbed()
					.setAuthor("Erreur PandaBot", bot.user.avatarURL)
					.setTitle(`PandaBot ne peut pas gérer une file d'attente possédant plus d'un morceau.`)
					.setColor("#C73E1D")
					.setFooter(message.author.username, message.author.avatarURL)
					message.channel.send({embed});
					message.delete();
				} else {
					message.delete();
					
					const embed = new Discord.RichEmbed()
					.setAuthor("Musique PandaBot", bot.user.avatarURL)
					.setTitle(`Morceau mis en attente`)
					.setDescription(videoID)
					.setColor("#BC8034")
					.setFooter(message.author.username, message.author.avatarURL)
					message.channel.send({embed});
					musicsAwaiting.set(message.guild.id, videoID);
				}
			}
		});
		message.delete();
	} else if (messageReduced.indexOf('pb!btm') == 0 || messageReduced.indexOf('pb!blindtestmode') == 0) {
		if (message.channel.type == 'text') {
			message.delete();
		}
		if (messageReduced.indexOf('pb!btm help') == 0 && message.channel.type == 'text') {

			const embed = new Discord.RichEmbed()
			.setAuthor("Aide PandaBot", bot.user.avatarURL)
			.setTitle(`Page d'aide sur le mode Blind Test`)
			.setDescription(`Le mode Blind Test peut être activé avec **pb!btm** ou **pb!blindtestmode**. Sachez aussi que vous ne pourrez pas utiliser la file d'attente, ni rechercher de musique dans ce mode. Avec ce mode, la différence sera que vous ne pourrez pas voir quelle a été la musique proposée. Pour mettre de la musique, vous devrez donc communiquer avec PandaBot en message privé, vous pourrez aussi le faire dans ce channel, mais si vos adversaires ont eu le temps de voir ce que vous avez mis, c'est tant pis pour vous... En message privé, vous devrez communiquer avec lui par **pb!btm play ${message.guild.id} [Lien Youtube]**, et par channel, **pb!play [Lien Youtube]**.`)
			.setColor("#9CADCE")
			.setFooter(`${message.author.username}`, message.author.avatarURL)
			message.channel.send({embed});
		} else if (messageReduced.indexOf('pb!btm play') != -1) {
			if (message.channel.type == 'dm') {
				var messageGuildId = messageReduced.substr(12, 18);
				if (bot.guilds.find('id', messageGuildId) != undefined) {
					var messageLengthReducedBTM = message.content.length - 31;
					var memberVoiceChannel = bot.guilds.find('id', messageGuildId).members.find('id', message.author.id).voiceChannel;
					ytPlay(message.content.substr(31, messageLengthReducedBTM), memberVoiceChannel, message);
				}
			} else {
				const embed = new Discord.RichEmbed()
				.setAuthor("Erreur PandaBot", bot.user.avatarURL)
				.setTitle(`Cette commande ne peut être utilisée qu'en message privé.`)
				.setColor("#C73E1D")
				.setFooter(message.author.username, message.author.avatarURL)
				message.channel.send({embed});
			}
		} else if (message.channel.type == 'text') {
			if (blindTestMode.get(message.guild.id) != undefined) {
				blindTestMode.delete(message.guild.id);

				const embed = new Discord.RichEmbed()
				.setAuthor("Fonctionnalité PandaBot", bot.user.avatarURL)
				.setTitle(`Mode Blind Test désactivé`)
				.setColor("#0CCE6B")
				.setFooter(`${message.author.username}`, message.author.avatarURL)
				message.channel.send({embed});
			} else {
				blindTestMode.set(message.guild.id, message.channel);
				const embed = new Discord.RichEmbed()
				.setAuthor("Fonctionnalité PandaBot", bot.user.avatarURL)
				.setTitle(`Mode Blind Test activé`)
				.setColor("#0CCE6B")
				.setFooter(`${message.author.username}`, message.author.avatarURL)
				message.channel.send({embed});
			}
		}
	} else if (messageReduced.indexOf('bonjour') != -1 || messageReduced.indexOf('salut') != -1 || messageReduced.indexOf('yo') != -1 && message.channel.type == 'text') {
        if (messageReduced.indexOf('pandabot') != -1) {
            message.channel.send(`Bonjour ${message.member} !`);
        }
    } else if (messageReduced.indexOf('donne') != -1 && messageReduced.indexOf('bambou') != -1 && messageReduced.indexOf('panda') != -1) {
		if (entierAleatoire(1, 3) == 1) {
			message.channel.send(`Il sent pas bon ton bambou ${message.author.username}.`);
		} else if (entierAleatoire(1, 3) == 2) {
			message.channel.send(`C'est pas terrible ça ${message.author.username}... T'as pas autre chose ?`);
		} else if (entierAleatoire(1, 3) == 3) {
			message.channel.send(`Mais c'est trop bon ${message.author.username} ! J'en veux d'autres !`);
		}
	} else if (messageReduced.indexOf('panda') != -1 && messageReduced.indexOf('ignore') != -1 && messageReduced.indexOf('moi') != -1) {
		if (messageReduced.indexOf('pas') != -1) {
			message.channel.send(`Mais jamais je ne t'ignorerai ${message.author.username} ! *(Sauf si t'oublies mes bambous)*`);
		} else {
			message.react('💨');
		}
	} else if (messageReduced.indexOf('merci') != -1 && messageReduced.indexOf('panda') != -1) {
		message.channel.send(`Mais c'est un plaisir ${message.author.username} !`);
	} else if (messageReduced.indexOf('mon avatar') != -1 || messageReduced.indexOf('ma photo') != -1 || messageReduced.indexOf('ma pdp') != -1) {
		if (messageReduced.indexOf('pandabot') != -1) {
			const embed = new Discord.RichEmbed()
			.setDescription(`**Tu veux ta photo ?** Tiens, en plus t'es beau dessus.`)
			.setColor("#279AF1")
			.setImage(message.author.avatarURL)
			message.channel.send({embed});
		}
	} else if (message.author.bot == true && messageReduced.indexOf('ftg') != -1 && message.channel.type == 'text') {
		message.channel.send(`Calmez-vous les bots, on est peinard. **ET MON BAMBOU DONNEZ-MOI MON BAMBOU !**`);
	} else if (message.content.bot == true && messageReduced.indexOf('kawai' && message.channel.type == 'text') != -1) {
		message.channel.send(`Alors, excusez-moi, mais le plus kawaii c'est moi ici.`);
	} else if (messageReduced.indexOf('panda') != -1 && message.author.id != '372074594435072002') {
		if (messageReduced.indexOf('aime') != -1 || messageReduced.indexOf('adore') != -1) {
			message.channel.send(`Moi aussi je t'adore ${message.author.username}... Tu veux un gros câlin de panda ?`);
		}
	} else if (messageReduced.indexOf(`qu'est-ce qui est blanc et noir`) != -1) {
		message.channel.send(`C'est moi bien sûr !`);
	}
});